# README #

# Bud Test 

Combines the output of two APIs (swapi.dev/api/planets & swapi.dev/api/people) and returns the population of each planet.

## Installation

Create a virtual environment

```bash
python3 -m venv venv
```
Activate it

```bash
source venv/bin/activate
```
Install dependencies 

```bash
pip install -r requirements.txt
```

## Usage
```bash
python main.py
```

Navigate to the url that is shown in the terminal