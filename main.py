import asyncio
import uvicorn
from fastapi import FastAPI
import requests
import math
import aiohttp
import pandas as pd
import json

app = FastAPI()
# stores list of page numbers from first request
planet_pages = []
people_pages = []

results_people = []
results_planets = []


# initial request to generate a list of the num of pages in each endpoint
def get_num_of_pages(endpoint):
    global people_pages, planet_pages

    if endpoint == 'people':
        results_people.clear()
    else:
        results_planets.clear()

    # make initial request
    response = requests.get(f'https://swapi.dev/api/{endpoint}/')
    json_response = response.json()

    # calculate num of pages based on count returned
    num_of_pages = int(math.ceil(int(json_response['count']) / 10))
    if endpoint == 'people':
        results_people.append(json_response)
        people_pages = list(range(2, num_of_pages + 1))
    else:
        results_planets.append(json_response)
        planet_pages = (list(range(2, num_of_pages + 1)))


async def get_people():
    # initial request
    get_num_of_pages('people')

    async with aiohttp.ClientSession() as session:
        tasks = get_tasks(session, 'people')
        responses = await asyncio.gather(*tasks)
        for response in responses:
            results_people.append(await response.json())


async def get_planets():
    # initial request
    get_num_of_pages('planets')

    async with aiohttp.ClientSession() as session:
        tasks = get_tasks(session, 'planets')
        responses = await asyncio.gather(*tasks)
        for response in responses:
            results_planets.append(await response.json())


# uses people_pages and planet_pages lists to create list of requests to be made
def get_tasks(session, endpoint):
    tasks = []
    print(people_pages)
    if endpoint == 'people':
        for page_num in people_pages:
            tasks.append(session.get(f'https://swapi.dev/api/{endpoint}/?page={page_num}', ssl=False))
    else:
        for page_num in planet_pages:
            tasks.append(session.get(f'https://swapi.dev/api/{endpoint}/?page={page_num}', ssl=False))

    return tasks


async def run_multiple_tasks(*args):
    input_coroutines = [args[0], args[1]]
    res = await asyncio.gather(*input_coroutines, return_exceptions=True)
    return res


@app.get("/")
async def get_population():
    await run_multiple_tasks(get_people(), get_planets())

    # store results in separate dataframes
    df_people = pd.DataFrame()
    df_planets = pd.DataFrame()

    for x in results_people:
        df_people = df_people.append(pd.json_normalize(x['results']), ignore_index=True)

    # rename homeworld to url in people dataframe
    df_people.drop('url', axis=1, inplace=True)
    df_people.rename(columns={'homeworld': 'url'}, inplace=True)

    for x in results_planets:
        df_planets = df_planets.append(pd.json_normalize(x['results']), ignore_index=True)

    # reduce planets dataframe to only unique values of url and name
    df_planets = df_planets[['url', 'name']]
    df_planets.columns = ['url', 'planet_name']
    df_planets.drop_duplicates(inplace=True)

    # join on common column: url and return count by planet name
    df = pd.merge(df_people, df_planets, on='url', how='inner')
    df = df['planet_name'].value_counts().rename_axis('planet_name').reset_index(name='population')
    return json.loads(df.set_index('planet_name').to_json())


if __name__ == '__main__':
    uvicorn.run("main:app", port=1111, host='127.0.0.1')